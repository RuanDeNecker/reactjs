![React JS](assets/images/React_Native_Logo.png)

# ReactJS Brief

## Introduction

Welcome to the second year of Interactive Development! This year we will be focusing on improving; programming skills, effectively modelling and processing data, learning to systematically test, validate, and
refactor code; designing software and interfaces to meet specific user-centric goals; and speaking about technical aspects with technical and non-technical people alike.

## The Brief

### Create a Component Based Data Visualization Dashboard

You will have to research an API that you can use to integrate into your React application to create an Interactive Dashboard.

#### Tips

* [SpaceX](https://docs.spacexdata.com/?version=latest)
* [Coin Desk](https://www.coindesk.com/coindesk-api)
* Financial Services
* Sports Api's

### Background and Context

* React Js is used to develop Single Page Applications (SPA's) with Javascript or TypeScript. It allows for writing very interactive UI's.

* ReactJS allows the developer to write front-end applications using a Component Based Design

* Instead of using templates you can develop components with javascript that can pass rich data to the front-end and manage the state of the components.

## Project Breakdown

### What you will do

* You will have to create a Single Page Application with ReactJS that uses and API (Application Programming Interface) to display and compare data for the user.

### Requirements

* The application should implement a data-heavy API.
* The application should show and compare data through the use of carefully selected graphs.
* The Application should allow users to select the data that they want to compare.
* You must implement Git Version control and also use GitHub or GitLab as your remote repository.

## Process Breakdown

### Phase 1: Planning

In your second-year studies, you will be assessed on your ability to plan and adjust the scope of your project based on the time available, and a realistic estimation of your own pace. In order to plan the scope of your project, write down all the features you’d like to include, and separate into four groups:

* Essential or MVP (Minimum Viable Product , or features without which the most basic version of the project cannot exist)
* Polish (making it look great, with excellent user experience)
* Nice-to-have (the first features to add if there’s time after completing the first two groups)
* Maybe in the future (features that may be too big for the time available, or aren’t as high a priority as the previous groups)

Choosing where to group each feature should be done with research and objectivity, and your findings should be documented in your rationale. Work on each group of features as separate phases in order, always checking in with yourself whether what you’re working on is part of the group you are currently processing.

Aim to complete the first two phases with the time available, and only move on to the third and fourth phases if you have extra time left.

Use your lists of features to create a rough schedule for what you will work on each week.

Record your research, planning and decisions in your rationale.

### Phase 2: Implementation Phase

* Keep the implementation of your project up to date in a well-documented repository on GitHub. You will be assessed on having frequent commits throughout the term. This is for your safety and industry-readiness.
* List your app’s features with descriptions as part of your documentation.
* Provide a step-by-step user guide in your documentation, with screenshots.
* Follow an agile approach to managing your project, iterating with short sprints towards milestones. Align your milestones with your class times to maximise the benefit from your lecturer’s feedback. You will be assessed throughout, on your execution of an agile work
ethic.

### Phase 3: Testing Phase

* WEEK 6: User-testing
An official user-testing session will be held during class time, in which fellow students will test your app and provide feedback on a semi-standardised feedback form that you will make available.
* Consider feedback received within the context of your overall timeline and development priorities, and act accordingly.
* Record insights received from feedback and your resulting decisions in the rationale.

### Phase 4: Final Phase

#### WEEK 7

Present your project to the class, including research and decisions, using the following soft guideline:

1. Introduction of app with feature overview
1. Demo (playing to presentation screen from device, with emulator as backup)
1. Details of app, i.e. technologies used, research and decisions made, challenges, lessons learnt
1. Get feedback (make notes)

#### WEEK 8

During class time, show and discuss your finalised app, process (incl.
challenges), and code, with your lecturer. Submit all required deliverables afterwards.

::: tip
Don’t forget to save often and create different versions, just in case! Back up your work! Lost work needs to be re-done!

A journal/workbook for all your research, process and planning will be required so that you can keep all your ideas in ONE place and take notes in class.
Do not forget to hand in your plagiarism and image reference lists. Your assignments cannot be marked without this documentation.
:::

## What you will learn

### By completing this project, learners will have learnt to...

* Develop using advanced JavaScript concepts
* Understand Single Page Applications
* Routing, API and HTTP
* Object Oriented Programming in JavaScript
* Application and codebase documentation
* Professionally present projects
* Manage their time for a project
* Manage the scope of a project

## Submission

1. Submit a link to your project’s well-documented repo on GitHub
1. Submit a **PDF** of your rationale
1. Submit a **PDF** of your presentation slides (exported from presentation software as PDF)
1. Submit a **video** to Google Classroom in which you demonstrate and verbally describe each feature of your app.

:::tip
__Make sure of the following__: Your repo must contain your **documentation as a readme**, and your **plagiarism form**; Your Google Classroom submission must be a **LINK** to your repo, a **VIDEO** demonstrating your features (use your voice), and the required **PDF’s**.
:::

## Criteria for Assessment

* Scope estimation
* Completeness
* Concept
* Aesthetic
* User experience
* Technical proficiency
* Agile workflow
* Documentation
* Version control (frequency + descriptions)

## Deliverables

### You will need to hand in the following

* A link to your project’s well-documented repo on GitHub
* A plagiarism form and an image reference list according to the latest specification, available in a folder on your GitHub repo.
* Your rationale in PDF format (include images, research, diagrams, good formatting)
* Your presentation slides in PDF format
* A video (on Google Classroom) in which you casually demonstrate and verbally describe each feature of your app.

## Term Schedule

|Weeks | Content |
|--|--|
|Week 1 | Planning and Research |
|Week 2 | Ideation and WireFraming |
|Week 3 | Designs and Mockups and personas |
|Week 4 | Code and Projects setup & Implementation Phase |
|Week 5 | Implementation Phase |
|Week 6 | Testing Phase |
|Week 7 | Final Phase |
|Week 8 | Final Phase (Submission) |

## Rubric

|Criteria | Weighting |
|--|--:|
| **SPRINT MEETINGS (AGILE) AND PROGRESS**      | **25%** |
| **PRESENTATION**                            | **20%** |
| Project Presentation (Week 7) <br /> (Impression, Clarity, Knowledge Demonstration) | 75% |
| Documentation + Rationale | 25% |
| **TERM PROJECT**                            | **55%** |
|  Scope Estimation                           | 10% |
|  Completeness                               | 20% |
|  Concept                                    | 15% |
|  Aesthetic                                  | 10% |
|  User experience                            | 15% |
|  Technical proficiency                      | 25% |
|  Version control (frequency + descriptions) | 5%  |

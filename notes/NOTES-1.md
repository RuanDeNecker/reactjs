# Notes 1

## Introduction to notes

The notes files are small bite-size notes on the topic of the term. These notes will contain guides and information on the direct outcomes of the term and any external resources and quides about extra technologies used throughout this term can be found in the resources folder. The notes are structure in small outcomes and do not correlate to weeks in the term. Some weeks can cover more than one note file other weeks can cover one note across two week but generally would not be more.

## Introduction to REACT JS

For the installation of the React JS Framework please visit the React.js Documentations as it will have the most up to date information.

We will be using `npx create-react-app` to scaffold a basic react project for us.

Officail Documentation:  
[React JS](https://create-react-app.dev/)  
[Create React App](https://create-react-app.dev/)

### Notes 1 TOC:

1. Component Based Development
1. Imperative vs Declarative Development
1. Encapsulation

## Component Based Development

What is component based development?
It is the technique and procedure of developing and designing software in components that are reusable. This would mean that you create and construct software via a programming language (in this case javascript) into reusable components. A good analogy would be to look at a car or almost any complex real world product. A car is a large assembly of parts, some are reusable or the same and others are custom. Nuts, Bolts, Wheels, seats etc. are all reusable and can be used mostly for a single purpose. Custom parts can be the engine block, the car body, or other internal parts. Note that the engine block can be a custom part within the car and is only used once as cars usually only require one engine, the engine block is, however, reusable across the cars of the same model. That means that even a custom part such as an engine block can be reused in other cars. The idea behind component based development is to speed up software development and help developers from reinventing the wheel every time that they come across a problem or a need.

React JS enforces this thinking and is a framework with a set of functions that enables us to develop reusable components through the use of JavaScript, HTML and CSS. Each component is developed and then called where it is needed. This is called Declarative development, more on this later.

Imagine a how a general website looks. There are pieces of the pages that stay the same as users navigate through the website (navigation elements, headers, footers), other pieces look the same but the content changes (blog posts, product view and detail components). These pieces that stay the same in some way or another means that we have the opportunity to share code and therefore develop reusable components.


## Imperative vs Declarative Programming

Imperative and Declarative is in essence two styles of programming that are interrelated that you can differentiate but one can also merge into the other in some cases.

### What is Imperative programming? 
It is when the piece of code describes in detail how something should happen.

```javascript
// a basic for loop
function getListOfNumbers(amount) {
  let list = []
  for (let i = 0; i < amount; i++) {
    list.push(`number ${i}`)
  }
  return list
}

```

As you can see above the code describes exactly how to create a list of numbers. This is Imperative programming, it is in a way the exact stepwise detail of how and action should be performed or how to get to an end result. It is basic programming as you understand it. It is a detailed specification of how you are solving a problem.
These are things like arithmetic, loops, if statements and even more complex mathematical solutions. It is the "HOW?" of programming.

### What is declarative programming then? 
It is a style or programming paradigm that expresses the logic of the computations without describing the control flow of the program. In basic terms it would be to ask the program to do something and in this case to get a list of numbers of specified length.

```javascript
let numberList = getListOfNumbers(10)
```

Declarative programming describes "WHAT?" it needs but does not show the exact procedures that the program has to go through to get to the answer of the problem.

There is a level of abstraction, where you do not have to know how the function is getting to the result only how to invoke the function with the right parameters where you need the result in your code.

This is analogous of using a library such as jquery or a framework like React JS, where these libraries or frameworks expose different functions that one can use for certain effects such as showing, hiding, updating or even animating objects in the DOM.

## Encapsulation

Encapsulation is concept that you have had experience with in the previous term of Advanced Javascript. It takes into account Imperative and Declarative paradigms discussed above. The idea of encapsulation is to hide data, variables and even logic and to wrap it all up into a reusable form of code that can be used by calling methods without having to know how exactly how it gets to the answer.

React JS allows us to write components where the logic may be Imperative but we use the components in a declarative way. So for instance you might develop a navigation component with all of its routing logic that you can call in any place of your application and it will render there with all of its functionality.

This enables us te create highly extensible component based frontends.
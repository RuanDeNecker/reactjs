![React JS](assets/images/React_Native_Logo.png)

# REACT JS

This term is all about developing a site with component based development and a modern framework - React JS

This term relies heavily on the knowledge that you gained from the previous term's Advanced Javascript.

## REACTJS

ReactJS is a JavaScript library developed by Facebook. It is used for building reusable UI components. React is a library for building composable user interfaces, it supports and encourages the creation of reusable UI components, which presents data that changes over time by using declarative syntax. React is an open-source javascript library that was created by Facebook and has grown in popularity the past few years.

ReactJS uses a **Virtual DOM** (Document Object Model) to allow for two-way binding with data. We will explore how this works throughout the term.

## What we will be using for this term

1. [REACT JS](https://reactjs.org/)
2. [NodeJs (LTS)](https://nodejs.org/en/)
3. [NPM](https://www.npmjs.com/)
4. [NPX](https://www.npmjs.com/package/npx)
5. [create-react-app](https://create-react-app.dev/docs/getting-started/)